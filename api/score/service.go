package score

import (
	"github.mheducation.com/course-delivery/interview/api/external/scoresdk"
	"github.mheducation.com/course-delivery/interview/api/external/usersdk"
)

type Service struct {
	api *scoresdk.Client
}

func NewService(api *scoresdk.Client) *Service {
	return &Service{api: api}
}

func (s *Service) GetScoresForUserList(users []usersdk.User) (map[string]int, error) {
	userIds := make([]string, len(users))
	for i, user := range users {
		userIds[i] = user.ID
	}
	scores, err := s.api.GetScoresByUserId(userIds)
	if err != nil {
		return nil, err
	}
	scoreMap := make(map[string]int, len(scores))
	for _, score := range scores {
		scoreMap[score.UserID] = score.Score
	}
	return scoreMap, nil
}

func (s *Service) GetGamesForUserId(userID string) ([]scoresdk.Game, error) {
	return s.api.GetGamesForUserId(userID)
}
