package score

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
)

func GetLeaderGamesFactory(scoreService *Service) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		games, err := scoreService.GetGamesForUserId(ctx.Param("id"))
		if err != nil {
			fmt.Println("score.GetLeaderGamesFactory, ", err)
			return err
		}
		return ctx.JSON(http.StatusOK, log.JSON{
			"data": games,
		})
	}
}
