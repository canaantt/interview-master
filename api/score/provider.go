package score

import (
	"github.com/sarulabs/di"
	"github.mheducation.com/course-delivery/interview/api/external"
	"github.mheducation.com/course-delivery/interview/api/external/scoresdk"
)

const (
	ServiceName = "score.service"
	GetLeaderGamesHandlerName = "score.get-leader-games"
)

func RegisterServices(ctn *di.Builder) error {
	return ctn.Add(
		di.Def{
			Name:  ServiceName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				api := ctn.Get(external.ScoreSdkClientName).(*scoresdk.Client)
				return NewService(api), nil
			},
			Close: nil,
		},
		di.Def{
			Name: GetLeaderGamesHandlerName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				service := ctn.Get(ServiceName).(*Service)
				return GetLeaderGamesFactory(service), nil
			},
			Close: nil,
		},
	)
}
