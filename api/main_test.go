// Test score service

package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"testing"
	"time"
)

type Game struct {
	UserID string `json:"userId"`
	Score  int    `json:"score"`
}

type Games struct {
	Data []Game
}

type Leader struct {
	UserID string `json:"id"`
	Name   string `json:"name"`
	Score  int    `json:"score"`
	Handle string `json:"handle"`
}

type Leaders struct {
	Data []Leader
}

var baseURL = "http://0.0.0.0:5678/v1/leaders"
var allLeaders []Leader
var allLeadersWithGames []Leader
var allLeadersWithNoGames []Leader
var oneUserNoGame Leader
var oneUserWithGames Leader

/*
	Testing Strategy for Issue 1 - distinguish the users that haven't played the game
	I. Issue1 related test -
		- TestGamesForUserNoGame
			Step 1: Get All the Leaders
					- [x] How to parse response.body.data to JSON array
			Step 2: Examine the first leader with score === -1
					- [x] extract the userID
					- [x] send UserID to API Call GetGamesForUserId() and examine the len(games) === 0

		- TestGamesScoreSum - check the accuracy of sum
			Step 1: Get All the games for a leader with real score
			Step 2: Add up all the scores from all the games for this user and evaluate the leader.TestGamesScoreSum

	II. Basic Tests:
			 - TestLeaderService
			 - TestFetchingGamesService
*/

func TestLeaderService(t *testing.T) {
	leaders := Leaders{}
	url := baseURL
	err := getJson(url, &leaders)
	if err != nil {
		t.Error("Error occurs during fetching all leaders. Error => ", err)
	} else {
		t.Log("How many leaders have we received ==> ", len(leaders.Data))
		allLeaders = leaders.Data
		allLeadersWithGames, allLeadersWithNoGames, oneUserNoGame, oneUserWithGames = fillGlbVars(allLeaders)
	}
}

func TestGamesForUserNoGame(t *testing.T) {
	games := Games{}
	url := baseURL + oneUserNoGame.UserID + "/games"
	err := getJson(url, &games)
	if err != nil {
		t.Error("Error occurs during fetching all games for one user. Error => ", err)
	} else if len(games.Data) != 0 {
		t.Error("The length of the games for this user should be 0. But getting => ", len(games.Data))
	} else {
		t.Log("The length of the games for this user is indeed 0")
	}
}

func TestFetchingGamesService(t *testing.T) {
	games := Games{}
	url := baseURL + "/" + oneUserWithGames.UserID + "/games"
	err := getJson(url, &games)
	if err != nil {
		t.Error("Error ")
	} else {
		t.Log("The length of games for this random users is ==> ", len(games.Data))
	}
}

func TestGamesScoreSum(t *testing.T) {
	games := Games{}
	gamesScore := 0
	url := baseURL + "/" + oneUserWithGames.UserID + "/games"
	err := getJson(url, &games)
	if err != nil {
		t.Error("Error occurs during fetching all games for one user. Error => ", err)
	} else {
		for _, game := range games.Data {
			gamesScore = gamesScore + game.Score
			fmt.Print(gamesScore)
		}
		if gamesScore != oneUserWithGames.Score {
			t.Error("Random User's score is not the sum of all the games score.")
		} else {
			t.Log("Random User's score is indeed the sum of all the games score!")
		}
	}
}

//#region Helper functions
var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func fillGlbVars(allLeaders []Leader) ([]Leader, []Leader, Leader, Leader) {
	var allLeadersWithGames []Leader
	var allLeadersWithNoGames []Leader
	var oneUserNoGame Leader
	var oneUserWithGames Leader
	for _, leader := range allLeaders {
		if leader.Score == -1 {
			allLeadersWithNoGames = append(allLeadersWithNoGames, leader)
		} else {
			allLeadersWithGames = append(allLeadersWithGames, leader)
		}
	}
	oneUserNoGame = allLeadersWithNoGames[rand.Intn(len(allLeadersWithNoGames)-1)]
	oneUserWithGames = allLeadersWithGames[rand.Intn(len(allLeadersWithGames))-1]
	return allLeadersWithGames, allLeadersWithNoGames, oneUserNoGame, oneUserWithGames
}

//#endregion
