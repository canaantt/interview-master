package leader

type Leader struct {
	UserID string `json:"id"`
	Name   string `json:"name"`
	Score  int    `json:"score"`
	Handle string `json:"handle"`
}
