package leader

import (
	"github.mheducation.com/course-delivery/interview/api/score"
	"github.mheducation.com/course-delivery/interview/api/user"
)

type Service struct {
	scoreService *score.Service
	userService  *user.Service
}

func NewService(scoreService *score.Service, userService *user.Service) *Service {
	return &Service{
		scoreService: scoreService,
		userService:  userService,
	}
}

func (s *Service) GetLeaders() ([]Leader, error) {

	userList, err := s.userService.GetUsers()
	if err != nil {
		return nil, err
	}
	scoreList, err := s.scoreService.GetScoresForUserList(userList)
	if err != nil {
		return nil, err
	}

	leaderList := make([]Leader, len(userList))
	for i, u := range userList {
		leaderList[i].UserID = u.ID
		leaderList[i].Handle = u.Handle
		leaderList[i].Name = u.Name
		userGames, _ := s.scoreService.GetGamesForUserId(u.ID)
		if len(userGames) == 0 {
			leaderList[i].Score = -1
		} else {
			if s, ok := scoreList[u.ID]; ok {
				leaderList[i].Score = s
			}
		}
	}

	return leaderList, nil
}
