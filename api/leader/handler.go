package leader

import (
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"github.mheducation.com/course-delivery/interview/api/score"
)

func GetLeadersHandlerFactory(leaderService *Service) echo.HandlerFunc {
	return func(ctx echo.Context) error {

		leaders, err := leaderService.GetLeaders()
		if err != nil {
			return err
		}
		return ctx.JSON(http.StatusOK, log.JSON{
			"data": leaders,
		})
	}
}

func GetLeaderGamesHandlerFactory(scoreService *score.Service) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		userID := ctx.Param("leaderid")
		games, err := scoreService.GetGamesForUserId(userID)
		if err != nil {
			return err
		}

		return ctx.JSON(
			http.StatusOK,
			log.JSON{
				"data": games,
			},
		)
	}
}
