package leader

import (
	"github.com/sarulabs/di"
	"github.mheducation.com/course-delivery/interview/api/score"
	"github.mheducation.com/course-delivery/interview/api/user"
)

const (
	ServiceName           = "leader.service"
	GetLeadersHandlerName = "leader.getLeadersHandler"
)

func RegisterServices(builder *di.Builder) error {
	return builder.Add(
		di.Def{
			Name:  ServiceName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				scoreService := ctn.Get(score.ServiceName).(*score.Service)
				userService := ctn.Get(user.ServiceName).(*user.Service)
				return NewService(scoreService, userService), nil
			},
			Close: nil,
		},
		di.Def{
			Name:  GetLeadersHandlerName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				service := ctn.Get(ServiceName).(*Service)
				return GetLeadersHandlerFactory(service), nil
			},
			Close: nil,
		},
	)
}
