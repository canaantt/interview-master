package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/sarulabs/di"
	"github.mheducation.com/course-delivery/interview/api/external"
	"github.mheducation.com/course-delivery/interview/api/leader"
	"github.mheducation.com/course-delivery/interview/api/score"
	"github.mheducation.com/course-delivery/interview/api/user"
)

const (
	AppName = "main.app"
	HandlersName = "main.handlers"
)

var providers = []serviceProvider{
	RegisterServices,
	external.RegisterServices,
	leader.RegisterServices,
	user.RegisterServices,
	score.RegisterServices,
}

type serviceProvider func(*di.Builder) error

func buildServiceContainer() (di.Container, error) {
	var builder *di.Builder
	var err error
	if builder, err = di.NewBuilder(di.App, di.Request); err != nil {
		return nil, err
	}
	for _, provider := range providers {
		if err = provider(builder); err != nil {
			return nil, err
		}
	}
	return builder.Build(), nil
}

func RegisterServices(builder *di.Builder) error {
	return builder.Add(
		di.Def{
			Name:  AppName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				e := echo.New()
				e.Use(
					middleware.CORS(),
					middleware.Logger(),
				)
				h := ctn.Get(HandlersName).(handlers)
				setRoutes(e, h)
				return e, nil
			},
			Close: nil,
		},
		di.Def{
			Name:  HandlersName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				return handlers{
					getLeaders: leader.GetLeadersHandlerFactory(ctn.Get(leader.ServiceName).(*leader.Service)),
					getGames: score.GetLeaderGamesFactory(ctn.Get(score.ServiceName).(*score.Service)),
				}, nil
			},
			Close: nil,
		},
	)
}
