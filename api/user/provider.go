package user

import (
	"github.com/sarulabs/di"
	"github.mheducation.com/course-delivery/interview/api/external"
	"github.mheducation.com/course-delivery/interview/api/external/usersdk"
)

const (
	ServiceName = "user.service"
)

func RegisterServices(ctn *di.Builder) error {
	return ctn.Add(
		di.Def{
			Name:  ServiceName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				api := ctn.Get(external.UserSdkClientName).(*usersdk.Client)
				return NewService(api), nil
			},
			Close: nil,
		},
	)
}
