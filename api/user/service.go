package user

import "github.mheducation.com/course-delivery/interview/api/external/usersdk"

type Service struct {
	api *usersdk.Client
}

func NewService(api *usersdk.Client) *Service {
	return &Service{api: api}
}

func (s *Service) GetUsers() ([]usersdk.User, error) {
	// Usually this
	return s.api.GetUsers()
}
