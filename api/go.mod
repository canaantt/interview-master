module github.mheducation.com/course-delivery/interview/api

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2 // indirect
	github.com/icza/dyno v0.0.0-20180601094105-0c96289f9585
	github.com/labstack/echo v0.0.0-20180911044237-1abaa3049251
	github.com/labstack/gommon v0.2.7
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/stretchr/testify v1.2.2
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/crypto v0.0.0-20180910181607-0e37d006457b // indirect
	golang.org/x/sys v0.0.0-20180909124046-d0be0721c37e // indirect
)
