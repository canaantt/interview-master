package link

import "net/url"

type Link struct {
	Type string `json:"type"`
	Rel string `json:"rel"`
	Href url.URL `json:"href"`
}
