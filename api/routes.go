package main

import (
	"github.com/labstack/echo"
)

type handlers struct {
	getLeaders echo.HandlerFunc
	getGames echo.HandlerFunc
}

func setRoutes(e *echo.Echo, h handlers) {
	e.GET("v1/leaders", h.getLeaders)
	e.GET("v1/leaders/:id/games", h.getGames)
}
