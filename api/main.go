package main

import (
	"fmt"
	"github.com/labstack/echo"
)

func main() {
	ctn, err := buildServiceContainer()
	if err != nil {
		fmt.Printf("Unable to build service container: %s", err.Error())
	}
	e := ctn.Get(AppName).(*echo.Echo)
	e.Start(":5678")
}
