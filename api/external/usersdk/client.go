package usersdk

import (
	"fmt"
	"sync"
)

type Client struct {
}

var (
	users  []User
	nextid = 1
	idlock = sync.RWMutex{}
)

func makeID() string {
	idlock.Lock()
	defer idlock.Unlock()
	id := fmt.Sprintf("urn:user:%d", nextid)
	nextid++
	return id
}

func (c *Client) GetUsers() ([]User, error) {
	// Return copy of data such that we can't manipulate the data from the caller
	dupe := make([]User, len(users))
	copy(dupe, users)
	return dupe, nil
}
