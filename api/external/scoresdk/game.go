package scoresdk

type Game struct {
	UserID string `json:"userId"`
	Score  int    `json:"score"`
}
