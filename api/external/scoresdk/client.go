package scoresdk

import "fmt"

type Client struct{}

// Returns a mapping of id to score for those ids that have scores
func (c *Client) GetScoresByUserId(ids []string) ([]Score, error) {
	scoreMap := map[string]int{}
	for _, game := range games {
		if prevScore, ok := scoreMap[game.UserID]; ok {
			fmt.Printf("Adding score of %d to total of %d for user %s\n", game.Score, scoreMap[game.UserID], game.UserID)
			scoreMap[game.UserID] = prevScore + game.Score
			continue
		}
		fmt.Printf("Registering score of %d for user %s\n", game.Score, game.UserID)
		scoreMap[game.UserID] = game.Score
	}
	var out []Score
	for userID, score := range scoreMap {
		out = append(out, Score{
			UserID: userID,
			Score:  score,
		})
	}
	return out, nil
}

func (c *Client) GetGamesForUserId(id string) ([]Game, error) {
	var out []Game
	for _, game := range games {
		if game.UserID == id {
			out = append(out, game)
		}
	}
	return out, nil
}
