package external

import (
	"github.com/sarulabs/di"
	"github.mheducation.com/course-delivery/interview/api/external/scoresdk"
	"github.mheducation.com/course-delivery/interview/api/external/usersdk"
)

const (
	UserSdkClientName  = "external.usersdk.client"
	ScoreSdkClientName = "external.scoresdk.client"
)

func RegisterServices(builder *di.Builder) error {
	return builder.Add(
		di.Def{
			Name:  UserSdkClientName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				return &usersdk.Client{}, nil
			},
			Close: nil,
		},
		di.Def{
			Name:  ScoreSdkClientName,
			Scope: di.App,
			Build: func(ctn di.Container) (interface{}, error) {
				return &scoresdk.Client{}, nil
			},
			Close: nil,
		},
	)

}
