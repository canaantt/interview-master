# Toy Project for Course Delivery

This project demonstrates a simple application utilizing an `Angular` frontend and a `go` backend.
These technologies are used by the Course Delivery team.

## Quick Start

The entire application (both front-end and back-end) can be run by performing a `docker-compose up -d` from the root project directory.
Live-reload is implemented for both the ui (using Angular's tooling) and the api (using codegansta/gin).
Thus changes to the code should trigger a re-compile of the respective project.

Note that the first `docker-compose up -d` will take a while (~15-30 seconds) because all of the dependencies need to be installed.

If you are on a **windows machine**, make sure that docker has access to your C drive by opening Docker Settings > Shared Drives. See Docker's [Getting Started on Windows](https://docs.docker.com/docker-for-windows/#shared-drives) guide for more detailed instructions. Misconfigured shared drives will result in an `starting container process caused "exec: \"./start.sh\": stat ./start.sh: no such file or directory": unknown` error.