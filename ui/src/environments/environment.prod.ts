import {Environment} from './definition';

export const environment: Environment = {
  production: true,
  apiUrl: 'https://prod.example.com',
};
