import {ActionReducerMap} from '@ngrx/store';
import {reducer as leaderReducer, State as LeaderState} from './leader/leader.reducers';

export interface State {
  leaders: LeaderState;
}

export const reducers: ActionReducerMap<State> = {
  leaders: leaderReducer,
};
