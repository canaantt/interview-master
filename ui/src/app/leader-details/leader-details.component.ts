import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Leader} from '../external/Leader';
import {Api} from '../external/Api';
import {Game} from '../external/Game';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-leader-details',
  templateUrl: './leader-details.component.html',
  styleUrls: ['./leader-details.component.scss'],
})
export class LeaderDetailsComponent implements OnInit, OnDestroy {

  @Input() leader: Leader;
  games: Array<Game> = [];
  private gameSubscription: Subscription | null = null;

  constructor(private api: Api) {
  }

  ngOnInit() {
    this.gameSubscription = this.api.getGamesByLeaderID(this.leader.id).subscribe(games => this.games = games);
  }

  ngOnDestroy() {
    if (this.gameSubscription !== null) {
      this.gameSubscription.unsubscribe();
    }
  }

}
