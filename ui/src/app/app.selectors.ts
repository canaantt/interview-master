import {State} from './app.reducers';

export const selectLeadersFeature = (state: State) => state.leaders;
