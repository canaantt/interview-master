import { Component, OnInit } from '@angular/core';
import { Leader } from './external/Leader';
import { Api } from './external/Api';
import { select, Store } from '@ngrx/store';
import { State } from './app.reducers';
import { Observable } from 'rxjs';
import { selectAllLeaders, selectCurrentLeaderId } from './leader/leader.selectors';
import { Select, Set } from './leader/leader.actions';
import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(leaders: Leader[], searchText: string): Leader[] {
    if (!leaders) return [];
    if (!searchText) return leaders;
    searchText = searchText.toLowerCase();
    return leaders.filter(leader => {
      return leader.name.toLowerCase().includes(searchText);
    });
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  leaders$: Observable<Leader[]>;
  currentLeaderId: string | null = null;
  searchText: string;

  constructor(private api: Api, private store: Store<State>) {
    this.leaders$ = store.pipe(select(selectAllLeaders));
    store.pipe(select(selectCurrentLeaderId)).subscribe(id => this.currentLeaderId = id);
  }

  ngOnInit(): void {
    this.api.getPeople().subscribe((leaders => this.store.dispatch(new Set(leaders))));
  }

  selectLeader(leader: null | Leader) {
    if (this.isSelected(leader)) {
      this.store.dispatch(new Select(null));
      return;
    }
    this.store.dispatch(new Select(leader));
  }

  isSelected(leader: Leader): boolean {
    return leader.id === this.currentLeaderId;
  }
}
