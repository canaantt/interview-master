import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/internal/operators';
import {isLeader, Leader} from './Leader';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {Game, isGame} from './Game';

@Injectable({
  providedIn: 'root',
  useFactory: (client: HttpClient) => new Api(environment.apiUrl, client),
  deps: [HttpClient]
})
export class Api {
  constructor(private baseUrl: string, private http: HttpClient) {
  }

  getPeople(): Observable<Array<Leader>> {
    return this.http.get(
      this.baseUrl + '/v1/leaders',
      {
        observe: 'body',
        responseType: 'json',
      },
    ).pipe(
      map(response => {
        const data = response['data'];
        if (!(data instanceof Array)) {
          console.error('Malformed response from api: data is not an array');
          return [];
        }
        return data.filter<Leader>((p: any): p is Leader => {
          if (isLeader(p)) {
            return true;
          }
          console.error('Malformed response from api: invalid person entity', p);
          return false;
        });
      })
    );
  }

  getGamesByLeaderID(id: string): Observable<Array<Game>> {
    return this.http.get(
      this.baseUrl + `/v1/leaders/${id}/games`,
      {
        observe: 'body',
        responseType: 'json',
      },
    ).pipe(
      map(response => {
        const data = response['data'];
        if (!(data instanceof Array)) {
          console.error('Malformed response from api: data is not an array');
          return [];
        }
        return data.filter<Game>((g: any): g is Game => {
          if (isGame(g)) {
            return true;
          }
          console.error('Malformed response from api: invalid game entity', g);
          return false;
        });
      })
    );
  }
}
