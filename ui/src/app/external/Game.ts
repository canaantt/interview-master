export interface Game {
  userId: string;
  score: number;
}

export function isGame(data: any): data is Game {
  return typeof data === 'object'
    && typeof data.userId === 'string'
    && typeof data.score === 'number';
}
