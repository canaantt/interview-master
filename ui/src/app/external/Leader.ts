export interface Leader {
  id: string;
  name: string;
  handle: string;
  score: number;
}

export function isLeader(value: any): value is Leader {
  return typeof value === 'object'
    && typeof value.id === 'string'
    && typeof value.name === 'string'
    && typeof value.handle === 'string'
    && typeof value.score === 'number';
}
