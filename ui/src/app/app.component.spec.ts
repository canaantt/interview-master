import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { LeaderComponent } from './leader/leader.component';
import { LeaderDetailsComponent } from './leader-details/leader-details.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StoreModule, Store } from '@ngrx/store';
import { reducers } from './app.reducers';


describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        LeaderComponent,
        LeaderDetailsComponent
      ],
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(reducers)
      ],
      providers: []
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Leader Board');
  }));

});
