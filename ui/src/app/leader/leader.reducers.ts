import {ActionReducer} from '@ngrx/store';
import {Leader} from '../external/Leader';
import {Select, Set} from './leader.actions';

export interface State {
  leaders: { [key: string]: Leader };
  selectedLeader: string | null;
}

const defaultState: State = {
  leaders: {},
  selectedLeader: null,
};

export const reducer: ActionReducer<State> = (state = defaultState, action): State => {
  if (action instanceof Set) {
    const newLeaders = {};
    action.leaders.forEach(leader => newLeaders[leader.id] = leader);
    return {
      ...state,
      leaders: newLeaders,
    };
  }

  if (action instanceof Select) {
    console.log(`select action: ${action.id}`);
    return {
      ...state,
      selectedLeader: action.id
    };
  }
  return state;
};
