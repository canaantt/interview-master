import {Action} from '@ngrx/store';
import {Leader} from '../external/Leader';

export enum LeaderActionTypes {
  SET = 'leader.set',
  SELECT = 'leader.select',
}

export class Set implements Action {
  readonly type = LeaderActionTypes.SET;

  constructor(readonly leaders: Array<Leader>) {
  }
}

export class Select implements Action {
  readonly type = LeaderActionTypes.SELECT;
  readonly id: string;

  constructor(leader: null|Leader) {
    this.id = leader === null ? null : leader.id;
  }
}
