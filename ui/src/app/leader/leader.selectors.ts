import {createSelector, MemoizedSelector} from '@ngrx/store';
import {selectLeadersFeature} from '../app.selectors';
import {State} from '../app.reducers';

export const selectAllLeaders = createSelector(
  selectLeadersFeature,
  state => Object.getOwnPropertyNames(state.leaders).map(id => state.leaders[id]),
);

export const selectCurrentLeaderId: MemoizedSelector<State, null | string> = createSelector(
  selectLeadersFeature,
  state => state.selectedLeader,
);
