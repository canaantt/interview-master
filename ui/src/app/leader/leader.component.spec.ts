import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LeaderComponent } from './leader.component';
import { AppComponent } from '../app.component';
import { LeaderDetailsComponent } from '../leader-details/leader-details.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StoreModule, Store } from '@ngrx/store';
import { reducers } from '../app.reducers';


describe('PersonComponent', () => {
  let component: LeaderComponent;
  let fixture: ComponentFixture<LeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
