import { Component, Input } from '@angular/core';
import { Leader } from '../external/Leader';

@Component({
    selector: 'app-leader',
    templateUrl: './leader.component.html',
    styleUrls: ['./leader.component.css']
})
export class LeaderComponent {
    @Input() leader: Leader | null = null;
}
