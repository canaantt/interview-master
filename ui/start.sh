#!/bin/bash

set -euo pipefail

echo "************************************"
echo "* npm is installing dependencies   *"
echo "* this may take a while and has no *"
echo "* notification indicator.          *"
echo "************************************"

npm install 2>&1
npm start
